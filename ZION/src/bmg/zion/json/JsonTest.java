package bmg.zion.json;

import java.io.File;
import java.io.FileInputStream;

import bmg.zion.json.JsonExceptions.JsonIOException;

public class JsonTest
{
	public static void main(String... strings)
	{
		try
		{
			String jsonString = "{\n" +
					"\"glossary\": {\n" +
					"\"title\": \"example glossary\",\n" +
					"\"GlossDiv\": {\n" +
					"\"title\": \"S\",\n" +
					"\"GlossList\": {\n" +
					"\"GlossEntry\": {\n" +
					"\"ID\": \"SGML\",\n" +
					"\"SortAs\": \"SGML\",\n" +
					"\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
					"\"Acronym\": \"SGML\",\n" +
					"\"Abbrev\": \"ISO 8879:1986\",\n" +
					"\"GlossDef\": {\n" +
					"\"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
					"\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
					"},\n" +
					"\"GlossSee\": \"markup\"\n" +
					"}\n" +
					"}\n" +
					"}\n" +
					"}\n" +
					"}\n";
			JsonInputStream stringStream = new JsonInputStream(jsonString);
			JsonObject stringRoot = Json.parse(stringStream);
			JsonObject glossDef = stringRoot.get("glossary").getValue();
			glossDef = glossDef.get("GlossDiv").getValue();
			glossDef = glossDef.get("GlossList").getValue();
			glossDef = glossDef.get("GlossEntry").getValue();
			glossDef = glossDef.get("GlossDef").getValue();
			String para = glossDef.get("para").getValue();
			System.out.println(para);

			JsonValue abbrev = stringRoot.getByNameRecursive("Abbrev");
			if (abbrev != null)
			{
				System.out.println((String) abbrev.getValue());
			}

			JsonInputStream fileStream = new JsonInputStream(new FileInputStream(new File("doc/example.json")), JsonInputStream.DEFAULT_CAPACITY);
			JsonObject fileRoot = Json.parse(fileStream);
			JsonObject glossary = fileRoot.get("glossary").getValue();
			String title = glossary.get("title").getValue();
			System.out.println(title);
		}
		catch (Exception e)
		{
			JsonIOException.reThrow(e);
		}
	}
}
