package bmg.zion.json;

public class JsonExceptions
{
	public static class JsonException extends RuntimeException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 2027576089346271371L;

		public JsonException()
		{
			super();
		}

		public JsonException(String message)
		{
			super(message);
		}

		public JsonException(Throwable thrown)
		{
			super(thrown);
		}

		public static void reThrow(Throwable thrown)
		{
			if (thrown instanceof JsonException)
			{
				throw (JsonException) thrown;
			}
			else
			{
				throw new JsonException(thrown);
			}
		}
	}

	public static class JsonParseException extends JsonException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -3913234615065429313L;

		public JsonParseException(String message)
		{
			super(message);
		}

		public JsonParseException(JsonToken.Type expected, JsonToken got)
		{
			super("Expected: " + expected + " but got " + got.toString());
		}
	}

	public static class JsonIOException extends JsonException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1618289582143937970L;

		public JsonIOException(String message)
		{
			super(message);
		}
	}
}
