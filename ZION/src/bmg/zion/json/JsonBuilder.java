package bmg.zion.json;

import bmg.zion.json.JsonExceptions.JsonParseException;

public class JsonBuilder
{
	private JsonInputStream input;
	private JsonToken current;

	public JsonBuilder(JsonInputStream input)
	{
		this.input = input;
		current = null;
	}

	public JsonToken getLastToken()
	{
		return current;
	}

	public JsonToken getNextToken()
	{
		return extract();
	}

	private JsonToken extract()
	{
		consumeWhitespace();

		if (input.getCurrentChar() == '{')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.OBJECT_BEGIN, "{", input.getLine(), input.getColumn());
			return current;
		}
		else if (input.getCurrentChar() == '}')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.OBJECT_END, "}", input.getLine(), input.getColumn());
			return current;
		}
		else if (input.getCurrentChar() == '[')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.ARRAY_BEGIN, "[", input.getLine(), input.getColumn());
			return current;
		}
		else if (input.getCurrentChar() == ']')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.ARRAY_END, "]", input.getLine(), input.getColumn());
			return current;
		}
		else if (input.getCurrentChar() == ':')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.COLON, ":", input.getLine(), input.getColumn());
			return current;
		}
		else if (input.getCurrentChar() == ',')
		{
			input.getNextChar();
			current = new JsonToken(JsonToken.Type.COMMA, ",", input.getLine(), input.getColumn());
			return current;
		}
		else if (Character.isDigit(input.getCurrentChar()) || input.getCurrentChar() == '+' || input.getCurrentChar() == '-')
		{
			return extractNumber();
		}
		else if (Character.isLetter(input.getCurrentChar()) || input.getCurrentChar() == '_')
		{
			return extractWord();
		}
		else if (input.getCurrentChar() == '"')
		{
			return extractString();
		}
		else
		{
			current = new JsonToken(JsonToken.Type.EOF, "End Of File", input.getLine(), input.getColumn());
			return current;
		}
	}

	private JsonToken extractNumber()
	{
		StringBuilder sb = new StringBuilder();
		int l = input.getLine();
		int c = input.getColumn();

		if (input.getCurrentChar() == '+' || input.getCurrentChar() == '-')
		{
			sb.append(input.getCurrentChar());
			input.getNextChar();
			consumeWhitespace();
		}

		while (Character.isDigit(input.getCurrentChar()))
		{
			sb.append(input.getCurrentChar());
			char next = input.getNextChar();
			if (next == 'e'
					|| next == 'E'
					|| next == '.'
					|| next == '+'
					|| next == '-')
			{
				sb.append(next);
				input.getNextChar();
				consumeWhitespace();
			}
		}

		sb.replace(0, sb.length(), "" + Double.parseDouble(sb.toString()));

		current = new JsonToken(JsonToken.Type.NUMBER, sb.toString(), l, c);
		return current;
	}

	private JsonToken extractString()
	{
		StringBuilder sb = new StringBuilder();
		int l = input.getLine();
		int c = input.getColumn();
		input.getNextChar(); // consume '"'
		while (input.getCurrentChar() != '"')
		{
			sb.append(input.getCurrentChar());
			input.getNextChar();
			if (input.getCurrentChar() == JsonInputStream.EOF)
			{
				throw new JsonParseException("Reached End Of File while parsing STRING");
			}
		}
		input.getNextChar(); // consume '"'
		current = new JsonToken(JsonToken.Type.STRING, sb.toString(), l, c);
		return current;
	}

	private JsonToken extractWord()
	{
		StringBuilder sb = new StringBuilder();
		int l = input.getLine();
		int c = input.getColumn();

		while (Character.isLetter(input.getCurrentChar()) || Character.isDigit(input.getCurrentChar()) || input.getCurrentChar() == '_')
		{
			sb.append(input.getCurrentChar());
			input.getNextChar();
		}

		if (sb.toString().toLowerCase() == "true" || sb.toString().toLowerCase() == "false")
		{
			current = new JsonToken(JsonToken.Type.BOOLEAN, sb.toString().toLowerCase(), l, c);
			return current;
		}
		else if (sb.toString().toLowerCase() == "null")
		{
			current = new JsonToken(JsonToken.Type.NULL, sb.toString().toLowerCase(), l, c);
			return current;
		}
		else
		{
			current = new JsonToken(JsonToken.Type.WORD, sb.toString(), l, c);
			return current;
		}
	}

	private void consumeWhitespace()
	{
		while (whitespace(input.getCurrentChar()))
		{
			input.getNextChar();
		}
	}

	private boolean whitespace(char c)
	{
		boolean result = (c == ' ' || c == '\n' || c == '\r' || c == '\t');
		return result;
	}
}
