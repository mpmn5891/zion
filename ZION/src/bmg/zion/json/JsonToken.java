package bmg.zion.json;

public class JsonToken
{
	public Type type;
	public String text;
	public int line;
	public int column;

	public JsonToken(Type type, String text, int line, int column)
	{
		this.type = type;
		this.text = text;
		this.line = line;
		this.column = column;
	}

	@Override
	public String toString()
	{
		return "JsonToken{ " + type + ", TEXT='" + text + "', at(" + line + "," + column + ")}";
	}

	public enum Type
	{
		OBJECT_BEGIN, // '['
		OBJECT_END, // ']'
		ARRAY_BEGIN, // '{'
		ARRAY_END, // '}'
		STRING, // '"'
		NUMBER, // 0-9
		BOOLEAN, // 1,0,true,false
		NULL, // null
		WORD, // a-z,A-Z
		COMMA, // ','
		COLON, // ':'
		EOF; // End Of File
	}
}
