package bmg.zion.json;

import java.util.HashMap;
import java.util.Map;

public class JsonObject extends HashMap<String, JsonValue>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5720302051637099149L;
	// simple extension

	public JsonValue getByNameRecursive(String name)
	{
		return getByNameRecursive(name, this);
	}

	private JsonValue getByNameRecursive(String name, JsonObject object)
	{
		for (Map.Entry<String, JsonValue> entry : object.entrySet())
		{
			if (entry.getKey().equals(name))
			{
				return entry.getValue();
			}
			if (entry.getValue().getType() == JsonValue.Type.OBJECT)
			{
				JsonValue value = getByNameRecursive(name, (JsonObject) entry.getValue().getValue());
				if (value != null)
				{
					return value;
				}
			}
		}
		return null;
	}
}
