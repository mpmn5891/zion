package bmg.zion.json;

public class JsonValue
{
	private Object value;
	private Type type;

	public JsonValue(Type type, Object value)
	{
		this.type = type;
		this.value = value;
	}

	public JsonValue(String value)
	{
		set(value);
	}

	public JsonValue(double value)
	{
		set(value);
	}

	public JsonValue(JsonObject value)
	{
		set(value);
	}

	public JsonValue(JsonArray value)
	{
		set(value);
	}

	public JsonValue(boolean value)
	{
		set(value);
	}

	public JsonValue()
	{
		set();
	}

	public JsonValue set(String value)
	{
		this.value = value;
		type = Type.STRING;
		return this;
	}

	public JsonValue set(double value)
	{
		this.value = value;
		type = Type.NUMBER;
		return this;
	}

	public JsonValue set(JsonObject value)
	{
		this.value = value;
		type = Type.OBJECT;
		return this;
	}

	public JsonValue set(JsonArray value)
	{
		this.value = value;
		type = Type.ARRAY;
		return this;
	}

	public JsonValue set(boolean value)
	{
		this.value = value;
		type = Type.BOOLEAN;
		return this;
	}

	public JsonValue set()
	{
		this.value = null;
		type = Type.NULL;
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T getValue()
	{
		return (T) value;
	}

	public Type getType()
	{
		return type;
	}

	public enum Type
	{
		STRING,
		NUMBER,
		OBJECT,
		ARRAY,
		BOOLEAN,
		NULL;
	}
}
