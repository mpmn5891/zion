package bmg.zion.json;

import bmg.zion.json.JsonExceptions.JsonParseException;

public class Json
{
	public static JsonObject parse(JsonInputStream input)
	{
		// create json builder
		JsonBuilder builder = new JsonBuilder(input);
		// create json root object
		JsonObject root = parseObject(builder);
		// expect EOF token
		if (builder.getLastToken().type != JsonToken.Type.EOF)
		{
			throw new JsonParseException(JsonToken.Type.EOF, builder.getLastToken());
		}
		// return root object
		return root;
	}

	private static JsonObject parseObject(JsonBuilder builder)
	{
		// create json object
		JsonObject object = new JsonObject();
		// check for null token
		if (builder.getLastToken() == null)
		{
			builder.getNextToken();
		}
		// expect object begin token
		if (builder.getLastToken().type != JsonToken.Type.OBJECT_BEGIN)
		{
			throw new JsonParseException(JsonToken.Type.OBJECT_BEGIN, builder.getLastToken());
		}
		// consume object begin token
		builder.getNextToken();
		// parse object properties
		parseProperties(builder, object);
		// expect object end token
		if (builder.getLastToken().type != JsonToken.Type.OBJECT_END)
		{
			throw new JsonParseException(JsonToken.Type.OBJECT_END, builder.getLastToken());
		}
		// consume object end token
		builder.getNextToken();
		// return object
		return object;
	}

	private static void parseProperties(JsonBuilder builder, JsonObject root)
	{
		// hold on to key token
		JsonToken key = builder.getLastToken();
		// expect string token
		if (key.type != JsonToken.Type.STRING)
		{
			throw new JsonParseException(JsonToken.Type.STRING, key);
		}
		// consume string token
		builder.getNextToken();
		// expect colon token
		if (builder.getLastToken().type != JsonToken.Type.COLON)
		{
			throw new JsonParseException(JsonToken.Type.COLON, builder.getLastToken());
		}
		// consume colon token
		builder.getNextToken();
		// get the string from key, parse a value, and put in object
		root.put(key.text, parseValue(builder));
		// if next token is a comma, then parse the next property
		if (builder.getLastToken().type == JsonToken.Type.COMMA)
		{
			// consume comma token
			builder.getNextToken();
			// recursive parse property
			parseProperties(builder, root);
		}
	}

	private static JsonArray parseArray(JsonBuilder builder)
	{
		// create json array
		JsonArray array = new JsonArray();
		// expect array begin token
		if (builder.getLastToken().type != JsonToken.Type.ARRAY_BEGIN)
		{
			throw new JsonParseException(JsonToken.Type.ARRAY_BEGIN, builder.getLastToken());
		}
		// consume array begin token
		builder.getNextToken();
		// loop until array end token in reached
		while (builder.getLastToken().type != JsonToken.Type.ARRAY_END)
		{
			// parse a value and add it to this array
			array.add(parseValue(builder));
			// check for comma
			if (builder.getLastToken().type == JsonToken.Type.COMMA)
			{
				// consume comma token
				builder.getNextToken();
				// check for value token
				switch (builder.getLastToken().type)
				{
					// exit switch for below cases and continue loop
					case STRING:
					case NUMBER:
					case BOOLEAN:
					case NULL:
					case OBJECT_BEGIN:
					case ARRAY_BEGIN:
					{
						break;
					}
					// throw exception because a value did not follow the comma
					default:
					{
						throw new JsonParseException("Expected a value token after comma but got " + builder.getLastToken().toString());
					}
				}
			}
			// if not a comma, then expect array end token
			else if (builder.getLastToken().type != JsonToken.Type.ARRAY_END)
			{
				throw new JsonParseException(JsonToken.Type.ARRAY_END, builder.getLastToken());
			}
		}
		// consume array end token
		builder.getNextToken();
		// return json array
		return array;
	}

	private static JsonValue parseValue(JsonBuilder builder)
	{
		// store token
		JsonToken token = builder.getLastToken();
		// test type
		switch (token.type)
		{
			case STRING:
			{
				builder.getNextToken();
				return new JsonValue(token.text);
			}
			case NUMBER:
			{
				builder.getNextToken();
				return new JsonValue(Double.parseDouble(token.text));
			}
			case BOOLEAN:
			{
				builder.getNextToken();
				return new JsonValue(Boolean.parseBoolean(token.text));
			}
			case NULL:
			{
				builder.getNextToken();
				return new JsonValue();
			}
			case ARRAY_BEGIN:
			{
				return new JsonValue(parseArray(builder));
			}
			case OBJECT_BEGIN:
			{
				return new JsonValue(parseObject(builder));
			}
			default:
			{
				throw new JsonParseException("Expected a value token but got " + builder.getLastToken().toString());
			}
		}
	}
}
