package bmg.zion.xml;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class XmlElement
{
	private String name;
	private String text;
	private Set<XmlAttribute> attributes;
	private List<XmlElement> children;
	private XmlElement parent;

	public XmlElement(XmlElement parent, String name)
	{
		this.parent = parent;
		this.name = name;
	}

	public void setAttribute(XmlAttribute attrib)
	{
		if (attrib != null)
		{
			if (attributes == null)
			{
				attributes = new LinkedHashSet<>();
			}
			attributes.add(attrib);
		}
	}

	public void addChild(XmlElement child)
	{
		if (child != null)
		{
			if (children == null)
			{
				children = new ArrayList<>();
			}
			children.add(child);
		}
	}

	public XmlAttribute getAttribute(String name)
	{
		if (attributes == null)
		{
			return null;
		}
		else
		{
			for (XmlAttribute attrib : attributes)
			{
				if (attrib.getName().equals(name))
				{
					return attrib;
				}
			}
			return null;
		}
	}

	public XmlElement getChildByName(String name)
	{
		if (children == null)
		{
			return null;
		}
		for (XmlElement child : children)
		{
			if (child.getName().equals(name))
			{
				return child;
			}
		}
		return null;
	}

	public XmlElement getChildByNameRecursive(String name)
	{
		if (children == null)
		{
			return null;
		}
		for (XmlElement child : children)
		{
			if (child.getName().equals(name))
			{
				return child;
			}
			XmlElement found = child.getChildByNameRecursive(name);
			if (found != null)
			{
				return found;
			}
		}
		return null;
	}

	public List<XmlElement> getChildrenByName(String name)
	{
		if (children == null)
		{
			return null;
		}
		List<XmlElement> result = new ArrayList<>();
		for (XmlElement child : children)
		{
			if (child.getName().equals(name))
			{
				result.add(child);
			}
		}
		return result.isEmpty() ? null : result;
	}

	public List<XmlElement> getChildrenByNameRecursive(String name)
	{
		List<XmlElement> result = new ArrayList<>();
		getChildrenByNameRecursive(name, result);
		return result.isEmpty() ? null : result;
	}

	private void getChildrenByNameRecursive(String name, List<XmlElement> result)
	{
		if (children == null)
		{
			return;
		}
		for (XmlElement child : children)
		{
			if (child.getName().equals(name))
			{
				result.add(child);
			}
			child.getChildrenByNameRecursive(name, result);
		}
	}

	public Set<XmlAttribute> getAttributes()
	{
		return attributes;
	}

	public List<XmlElement> getChildren()
	{
		return children;
	}

	public XmlElement getParent()
	{
		return parent;
	}

	public String getName()
	{
		return name;
	}

	public String getText()
	{
		return text;
	}

	@Override
	public String toString()
	{
		return "XmlElement{" + name + ", contains "
				+ (getChildren() == null ? "0" : getChildren().size()) + " children and "
				+ (getAttributes() == null ? "0" : getAttributes().size()) + " attributes}";
	}
}
