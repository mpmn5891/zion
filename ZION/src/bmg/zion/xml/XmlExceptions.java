package bmg.zion.xml;

public class XmlExceptions
{
	public static class XmlException extends RuntimeException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 4591449746525201759L;

		public XmlException()
		{
			super();
		}

		public XmlException(String message)
		{
			super(message);
		}

		public XmlException(Throwable thrown)
		{
			super(thrown);
		}

		public static void reThrow(Throwable thrown)
		{
			if (thrown instanceof XmlException)
			{
				throw (XmlException) thrown;
			}
			else
			{
				throw new XmlException(thrown);
			}
		}
	}

	public static class XmlParseException extends XmlException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -8728559006668391366L;

		public XmlParseException(String message)
		{
			super(message);
		}

		public XmlParseException(XmlToken.Type expected, XmlToken got)
		{
			super("Expected: " + expected + ", but got: " + got.toString());
		}
	}

	public static class XmlIOException extends XmlException
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 4855697178237987832L;

		public XmlIOException(String message)
		{
			super(message);
		}
	}
}
