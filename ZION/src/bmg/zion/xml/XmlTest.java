package bmg.zion.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class XmlTest
{
	public static void main(String... strings)
	{
		try
		{
			XmlInputStream stream = new XmlInputStream(new FileInputStream(new File("doc/example.xml")), XmlInputStream.DEFAULT_CAPACITY);
			XmlElement root = Xml.parse(stream);
			printElement(root);

			System.out.println(root.getChildByName("zion").getAttribute("version").getValue());

			XmlElement cdata = root.getChildByNameRecursive("CDATA");
			if (cdata != null)
			{
				XmlElement element = Xml.parse(cdata.getAttribute("text").getValue(), true);
				System.out.println(cdata.getAttribute("text").getValue());
				printElement(element);
			}
			else
			{
				System.out.println("cdata was null");
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public static void printElement(XmlElement root)
	{
		System.out.println(root.toString());
		if (root.getAttributes() != null)
		{
			for (XmlAttribute attribute : root.getAttributes())
			{
				System.out.println(attribute.toString());
			}
		}
		if (root.getChildren() != null)
		{
			for (XmlElement element : root.getChildren())
			{
				printElement(element);
			}
		}
	}
}
