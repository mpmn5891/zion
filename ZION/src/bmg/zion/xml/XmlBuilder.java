package bmg.zion.xml;

import java.io.InputStream;

import bmg.zion.xml.XmlExceptions.XmlParseException;

public class XmlBuilder
{
	private XmlInputStream input;
	private XmlToken current;
	private boolean textMode;
	private boolean cdataMode;
	private boolean commentMode;
	private final boolean verbose;

	public XmlBuilder(InputStream input)
	{
		this(input, false);
	}

	public XmlBuilder(InputStream input, boolean verbose)
	{
		this(new XmlInputStream(input, XmlInputStream.DEFAULT_CAPACITY), verbose);
	}

	public XmlBuilder(XmlInputStream input)
	{
		this(input, false);
	}

	public XmlBuilder(XmlInputStream input, boolean verbose)
	{
		this.input = input;
		this.verbose = verbose;
	}

	public XmlToken extract()
	{
		if (commentMode) // parse comments, only enters if in verbose mode
		{
			System.out.println("XB:comment mode");
			if (input.getCurrentChar() == '-')
			{
				// could be the end of a comment, or just part of the comment content
				if (follows("-->"))
				{
					// this is a comment end tag, exit comment mode
					expect("->");
					commentMode = false;
					current = new XmlToken(XmlToken.Type.COMMENT_END, "-->", input.getLine(), input.getColumn() - 3, input.getColumn());
					System.out.println(current.toString());
					return current;
				}
			}
			else
			{
				StringBuilder builder = new StringBuilder();
				int startLine = input.getLine();
				int startColumn = input.getColumn();
				while (true)
				{
					if (input.getCurrentChar() == '-')
					{
						if (follows("-->"))
						{
							break;
						}
					}
					builder.append(input.getCurrentChar());
					input.getNextChar();
				}
				String value = builder.toString()
						.replaceAll("&lt;", "<")
						.replaceAll("&gt;", ">")
						.replaceAll("&apos;", "'")
						.replaceAll("&quot;", "\"")
						.replaceAll("&amp;", "&");
				current = new XmlToken(XmlToken.Type.TEXT, value, startLine, input.getLine(), startColumn, input.getColumn());
				System.out.println(current.toString());
				return current;
			}
		}
		else if (textMode) // parse text, which could be element names, attributes, or strings
		{
			System.out.println("XB:text mode");
			// consume whitespace
			while (whitespace(input.getCurrentChar()))
			{
				input.getNextChar();
			}

			if (input.getCurrentChar() == '/')
			{
				// could be a simple close tag />, check next character for >
				if (input.peekNextChar() == '>')
				{
					// this is a simple close tag exit text mode
					input.getNextChar(); // '>'
					textMode = false;
					input.getNextChar(); // consume '>'
					current = new XmlToken(XmlToken.Type.SIMPLE_TAG_CLOSE, "/>", input.getLine(), input.getColumn() - 2, input.getColumn());
					System.out.println(current.toString());
					return current;
				}
			}

			if (input.getCurrentChar() == '?')
			{
				// could be a meta close tag '?>', check next character for '>'
				if (input.peekNextChar() == '>')
				{
					// this is a meta close tag, exit text mode
					input.getNextChar(); // '>'
					textMode = false;
					input.getNextChar(); // consume '>'
					current = new XmlToken(XmlToken.Type.META_END, "?>", input.getLine(), input.getColumn() - 2, input.getColumn());
					System.out.println(current.toString());
					return current;
				}
			}

			if (input.getCurrentChar() == '=')
			{
				// equals, continue text mode, expect STRING
				input.getNextChar(); // consume '='
				current = new XmlToken(XmlToken.Type.EQUALS, "=", input.getLine(), input.getColumn() - 1, input.getColumn());
				System.out.println(current.toString());
				return current;
			}

			if (input.getCurrentChar() == '>')
			{
				// this is an end tag, exit text mode
				textMode = false;
				input.getNextChar(); // consume '>'
				current = new XmlToken(XmlToken.Type.TAG_END, ">", input.getLine(), input.getColumn() - 1, input.getColumn());
				System.out.println(current.toString());
				return current;
			}

			if (input.getCurrentChar() == '"' || input.getCurrentChar() == '\'')
			{
				// reading a STRING, continue until same STRING character is reached
				char stringBeginCharacter = input.getCurrentChar();
				int lineStart = input.getLine();
				int columnStart = input.getColumn();
				StringBuilder builder = new StringBuilder();
				while (input.peekNextChar() != stringBeginCharacter)
				{
					builder.append(input.getNextChar());
				}
				expect("" + stringBeginCharacter);
				String value = builder.toString()
						.replaceAll("&lt;", "<")
						.replaceAll("&gt;", ">")
						.replaceAll("&apos;", "'")
						.replaceAll("&quot;", "\"")
						.replaceAll("&amp;", "&");
				current = new XmlToken(XmlToken.Type.STRING, value, lineStart, input.getLine(), columnStart, input.getColumn());
				System.out.println(current.toString());
				return current;
			}

			// check for an alpha character or '_'
			if (Character.isAlphabetic(input.getCurrentChar()) || input.getCurrentChar() == '_')
			{
				// read NAME until non alpha-numeric or whitespace
				StringBuilder builder = new StringBuilder();
				int startLine = input.getLine();
				int startColumn = input.getColumn();
				builder.append(input.getCurrentChar());
				while (Character.isLetterOrDigit(input.peekNextChar()) || input.peekNextChar() == '_' || input.peekNextChar() == ':')
				{
					builder.append(input.getNextChar());
					if (input.peekNextChar() == ':')
					{
						throw new XmlParseException("Undefined namespace separator ':' at " + input.getLine() + ", " + input.getColumn());
					}
				}
				input.getNextChar(); // consume
				current = new XmlToken(XmlToken.Type.NAME, builder.toString(), startLine, input.getLine(), startColumn, input.getColumn());
				System.out.println(current.toString());
				return current;
			}

			throw new XmlParseException("Expected a NAME or STRING token, got: " + input.getCurrentChar());
		}
		else if (cdataMode) // parse cdata
		{
			System.out.println("XB:cdata mode");
			if (input.getCurrentChar() == ']')
			{
				// could be cdata end tag ]]>
				if (follows("]]>"))
				{
					// this is a cdata end tag, exit cdata mode
					expect("]>");
					cdataMode = false;
					current = new XmlToken(XmlToken.Type.CDATA_END, "]]>", input.getLine(), input.getColumn() - 3, input.getColumn());
					System.out.println(current.toString());
					return current;
				}
			}
			else
			{
				StringBuilder builder = new StringBuilder();
				int lineStart = input.getLine();
				int columnStart = input.getColumn();
				while (true)
				{
					if (input.getCurrentChar() == ']')
					{
						if (follows("]]>"))
						{
							break;
						}
					}
					builder.append(input.getCurrentChar());
					input.getNextChar();
				}
				String value = builder.toString()
						.replaceAll("&lt;", "<")
						.replaceAll("&gt;", ">")
						.replaceAll("&apos;", "'")
						.replaceAll("&quot;", "\"")
						.replaceAll("&amp;", "&");
				current = new XmlToken(XmlToken.Type.TEXT, value, lineStart, input.getLine(), columnStart, input.getColumn());
				System.out.println(current.toString());
				return current;
			}
		}
		else // if nothing else assume we are looking for a new tag
		{
			System.out.println("XB:tag mode");
			// consume whitespace
			while (whitespace(input.getCurrentChar()))
			{
				input.getNextChar();
			}

			if (input.getCurrentChar() == '<')
			{
				if (input.peekNextChar() == '!')
				{
					// could be cdata <![CDATA[, or comment <!--
					input.getNextChar(); // '!'
					if (input.peekNextChar() == '[')
					{
						// reading cdata
						int start = input.getColumn() - 1;
						expect("[CDATA[");
						cdataMode = true;
						current = new XmlToken(XmlToken.Type.CDATA_BEGIN, "<![CDATA[", input.getLine(), start, input.getColumn());
						System.out.println(current.toString());
						return current;
					}
					else
					{
						// reading a comment
						expect("--");
						// if verbose then read the comment tags and content
						if (verbose)
						{
							// set comment mode to read comment as a STRING token
							commentMode = true;
							current = new XmlToken(XmlToken.Type.COMMENT_BEGIN, "<!--", input.getLine(), input.getColumn() - 4, input.getColumn());
							System.out.println(current.toString());
							return current;
						}
						else // otherwise discard comment
						{
							while (true)
							{
								if (input.peekNextChar() == '-')
								{
									input.getNextChar();
									if (input.peekNextChar() == '-')
									{
										input.getNextChar();
										if (input.peekNextChar() == '>')
										{
											input.getNextChar();
											break;
										}
									}
								}
								input.getNextChar(); // consume
							}
							input.getNextChar();
							return extract(); // extract the next token
						}
					}
				}

				if (input.peekNextChar() == '?')
				{
					// reading meta data
					input.getNextChar(); // '?'
					textMode = true;
					// consume '?'
					input.getNextChar();
					current = new XmlToken(XmlToken.Type.META_BEGIN, "<?", input.getLine(), input.getColumn() - 1, input.getColumn());
					System.out.println(current.toString());
					return current;
				}

				if (input.peekNextChar() == '/')
				{
					// reading end tag
					input.getNextChar(); // '/'
					// get next char for next run of extract
					input.getNextChar();
					textMode = true;
					current = new XmlToken(XmlToken.Type.TAG_CLOSE, "</", input.getLine(), input.getColumn() - 3, input.getColumn());
					System.out.println(current.toString());
					return current;
				}

				// if nothing else then reading an open tag
				textMode = true;
				input.getNextChar(); // '<'
				current = new XmlToken(XmlToken.Type.TAG_BEGIN, "<", input.getLine(), input.getColumn() - 1, input.getColumn());
				System.out.println(current.toString());
				return current;
			}
		}

		if (input.getCurrentChar() == XmlInputStream.EOF)
		{
			current = new XmlToken(XmlToken.Type.EOF, "", input.getLine(), input.getColumn(), input.getColumn());
			System.out.println(current.toString());
			return current;
		}

		return null;
	}

	public XmlToken getLastToken()
	{
		return current == null ? null : current;
	}

	public boolean isVerbose()
	{
		return verbose;
	}

	private void expect(String expects)
	{
		char ch = input.getNextChar();
		for (char c : expects.toCharArray())
		{
			if (ch != c)
			{
				throw new XmlParseException("Expected '" + c + "' at " + input.getLine() + ", " + input.getColumn() + " but got '" + ch + "'");
			}
			ch = input.getNextChar();
		}
	}

	private boolean follows(String text)
	{
		char[] peek = input.peekNextChar(text.length());
		for (int i = 0; i < peek.length; i++)
		{
			if (peek[i] != text.charAt(i))
			{
				System.out.println("Follows:" + text + ":FALSE");
				return false;
			}
		}
		System.out.println("Follows:" + text + ":TRUE");
		return true;
	}

	private boolean whitespace(char c)
	{
		boolean result = (c == ' ' || c == '\t' || c == '\r' || c == '\n');
		System.out.println("XB:Whitespace:" + result);
		return result;
	}
}
