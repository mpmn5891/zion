package bmg.zion.xml;

import java.io.IOException;
import java.io.InputStream;

import bmg.zion.xml.XmlExceptions.XmlIOException;

public class XmlInputStream
{
	public static final char EOF = '\0';
	public static final int DEFAULT_CAPACITY = 128;

	private InputStream input;
	private byte[] buffer;
	private int line = 0;
	private int column = 0;
	private int index = -1;
	private boolean endReached = false;

	// creates an XmlInputStream backed by an InputStream
	public XmlInputStream(InputStream input, int bufferCapacity)
	{
		this.input = input;
		buffer = new byte[bufferCapacity];
		// fill buffer to capacity
		try
		{
			int bytesRead = input.read(buffer);
			if (bytesRead < buffer.length)
			{
				buffer[bytesRead] = (int) EOF;
				endReached = true;
			}
		}
		catch (IOException ioe)
		{
			XmlIOException.reThrow(ioe);
		}
	}

	// creates an XmlInputStream backed by a String, useful when you have already read in XML data as a String and want to parse it
	public XmlInputStream(String xmlString)
	{
		this.input = null;
		endReached = true;
		buffer = new byte[xmlString.length() + 1];
		int i = 0;
		while (i < xmlString.length())
		{
			buffer[i] = (byte) xmlString.charAt(i);
			i++;
		}
		buffer[i] = EOF;
	}

	public char getCurrentChar()
	{
		if (index == -1)
		{
			index = 0;
		}
		char c = (char) buffer[index];
		System.out.println("XIS:Current[" + c + "]");
		return c;
	}

	public char getNextChar()
	{
		index++;
		column++;
		if (index >= buffer.length)
		{
			refillBuffer();
		}
		char result = getCurrentChar();
		if (result == '\n' || result == '\r')
		{
			column = 0;
			line++;
		}
		System.out.print("XIS:Next:");
		return result;
	}

	public char peekNextChar()
	{
		if (index + 1 > buffer.length - 1)
		{
			refillBuffer();
		}
		char c = (char) buffer[index + 1];
		System.out.println("XIS:Peek[" + c + "]");
		return c;
	}

	public char[] peekNextChar(int amount)
	{
		if (amount > buffer.length - 1)
		{
			throw new XmlIOException("Can not peek " + amount + ", past buffers peekable capacity " + (buffer.length - 1));
		}
		if (index + amount > buffer.length - 1)
		{
			refillBuffer();
		}
		char[] result = new char[amount];
		for (int i = 0; i < amount; i++)
		{
			result[i] = (char) buffer[index + i];
			if (result[i] == EOF)
			{
				// no need to peek further than EOF
				break;
			}
		}
		System.out.println("XIS:PeekAmount[" + String.valueOf(result) + "]");
		return result;
	}

	public int getLine()
	{
		return line;
	}

	public int getColumn()
	{
		return column;
	}

	public InputStream getInputStream()
	{
		return input;
	}

	public boolean endReached()
	{
		return endReached;
	}

	private void refillBuffer()
	{
		if (endReached)
		{
			// end of file is already in buffer, no data to refill with
			return;
		}
		try
		{
			System.out.println("XIS:Refilled");
			// working buffer
			byte[] tempBuffer = new byte[buffer.length];
			for (int i = 0; i < (buffer.length - 1) - index; i++)
			{
				// move data remaining in buffer after index to the front of the working buffer
				tempBuffer[i] = buffer[i + index];
			}
			// read new data into the end of working buffer after index preserving previous data before index
			int bytesRead = input.read(tempBuffer, (tempBuffer.length) - index, index);
			if (bytesRead < index)
			{
				// if the number of read bytes is less than the buffers capacity past index then the end of available data has been reached
				// add EOF character to the end of the data
				tempBuffer[((tempBuffer.length) - index) + (bytesRead == -1 ? 0 : bytesRead)] = (int) EOF;
				endReached = true;
			}
			// buffer gets working buffer
			buffer = tempBuffer;
			// reset index
			index = 0;
		}
		catch (IOException ioe)
		{
			XmlIOException.reThrow(ioe);
		}
	}
}
