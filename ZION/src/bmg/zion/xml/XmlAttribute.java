package bmg.zion.xml;

public class XmlAttribute
{
	private String name;
	private String value;

	public XmlAttribute(String name, String value)
	{
		this.name = name;
		this.value = value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getName()
	{
		return name;
	}

	public String getValue()
	{
		return value;
	}

	@Override
	public String toString()
	{
		return "XmlAttribute{name='" + name
				+ "', value='" + value + "'}";
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		XmlAttribute attribute = (XmlAttribute) o;

		return (name != null ? name.equals(attribute.name) : attribute.name == null) &&
				(value != null ? value.equals(attribute.value) : attribute.value == null);
	}

	@Override
	public int hashCode()
	{
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		return result;
	}
}
