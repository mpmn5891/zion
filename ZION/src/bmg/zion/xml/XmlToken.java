package bmg.zion.xml;

public class XmlToken
{
	public Type type;
	public String text;
	public int lineStart;
	public int lineEnd;
	public int columnStart;
	public int columnEnd;

	public XmlToken(Type type, String text, int line, int columnStart, int columnEnd)
	{
		this(type, text, line, line, columnStart, columnEnd);
	}

	public XmlToken(Type type, String text, int lineStart, int lineEnd, int columnStart, int columnEnd)
	{
		this.type = type;
		this.text = text;
		this.lineStart = lineStart;
		this.lineEnd = lineEnd;
		this.columnStart = columnStart;
		this.columnEnd = columnEnd;
	}

	@Override
	public String toString()
	{
		return "XmlToken{type=" + type
				+ ", text=" + text
				+ ", Start(" + lineStart
				+ ":" + columnStart
				+ "), End(" + lineEnd
				+ ":" + columnEnd + ")}";
	}

	public enum Type
	{
		TAG_BEGIN, // <
		TAG_END, // >
		TAG_CLOSE, // </
		SIMPLE_TAG_CLOSE, // />
		NAME, // alpha-numeric
		TEXT, // any character data
		STRING, // alpha-numeric
		EQUALS, // =
		CDATA_BEGIN, // <![CDATA[
		CDATA_END, // ]]>
		COMMENT_BEGIN, // <!--
		COMMENT_END, // -->
		META_BEGIN, // <?
		META_END, // ?>
		EOF; // \0
	}
}
