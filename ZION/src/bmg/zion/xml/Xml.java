package bmg.zion.xml;

import java.io.InputStream;

import bmg.zion.xml.XmlExceptions.XmlParseException;

public class Xml
{
	private Xml()
	{
		// nothing to see here, prevents instantiation
	}

	public static XmlElement parse(String xmlString)
	{
		return Xml.parse(xmlString, false);
	}

	public static XmlElement parse(String xmlString, boolean verbose)
	{
		return Xml.parse(new XmlInputStream(xmlString), verbose);
	}

	public static XmlElement parse(InputStream input)
	{
		return Xml.parse(input, XmlInputStream.DEFAULT_CAPACITY);
	}

	public static XmlElement parse(InputStream input, int bufferCapacity)
	{
		return Xml.parse(input, bufferCapacity, false);
	}

	public static XmlElement parse(InputStream input, int bufferCapacity, boolean verbose)
	{
		return Xml.parse(new XmlInputStream(input, bufferCapacity), verbose);
	}

	public static XmlElement parse(XmlInputStream input)
	{
		return Xml.parse(input, false);
	}

	public static XmlElement parse(XmlInputStream input, boolean verbose)
	{
		XmlBuilder builder = new XmlBuilder(input, verbose);
		XmlElement root = new XmlElement(null, "root");
		Xml.parse(builder, root);
		if (builder.getLastToken().type != XmlToken.Type.EOF)
		{
			throw new XmlParseException(XmlToken.Type.EOF, builder.getLastToken());
		}
		return root;
	}

	private static XmlElement parse(XmlBuilder builder, XmlElement root)
	{
		// get last token from builder
		XmlToken token = builder.getLastToken();

		// edge case: if this is the first run token will be null
		if (token == null)
		{
			token = builder.extract();
		}

		// loop until end is reached
		while (token.type != XmlToken.Type.EOF)
		{
			XmlElement child = Xml.parseElement(builder, root);
			if (child != null)
			{
				root.addChild(child);
			}
			else
			{
				System.out.println("Child was NULL");
			}
			token = builder.getLastToken();
		}

		return root;
	}

	private static XmlElement parseElement(XmlBuilder builder, XmlElement root)
	{
		XmlToken token = builder.getLastToken();

		if (token.type == XmlToken.Type.TAG_BEGIN)
		{
			// consume the tag begin tag
			token = builder.extract();

			// expect a name tag
			if (token.type != XmlToken.Type.NAME)
			{
				throw new XmlParseException(XmlToken.Type.NAME, token);
			}

			// get contents of name tag
			String elementName = token.text;

			// consume name tag
			token = builder.extract();

			// create element
			XmlElement element = new XmlElement(root, elementName);

			// if a name tag then enter loop to parse attributes
			while (token.type == XmlToken.Type.NAME)
			{
				// get contents of name tag
				String attribute = token.text;

				// consume the name tag
				token = builder.extract();

				// expect equals tag
				if (token.type != XmlToken.Type.EQUALS)
				{
					throw new XmlParseException(XmlToken.Type.EQUALS, token);
				}

				// consume equals tag
				token = builder.extract();

				// expect a string tag
				if (token.type != XmlToken.Type.STRING)
				{
					throw new XmlParseException(XmlToken.Type.STRING, token);
				}

				// get contents of string tag
				String value = token.text;

				// create and add attribute
				XmlAttribute attrib = new XmlAttribute(attribute, value);
				element.setAttribute(attrib);

				// consume string tag, will loop if token is name tag, will exit loop otherwise
				token = builder.extract();
			}

			// if simple close tag, no body so do not check for nested elements
			if (token.type == XmlToken.Type.SIMPLE_TAG_CLOSE)
			{
				// consume simple close tag
				token = builder.extract();
			}
			// if end tag then loop to parse nested elements
			else if (token.type == XmlToken.Type.TAG_END)
			{
				// consume end tag
				token = builder.extract();

				// loop until close tag
				while (token.type != XmlToken.Type.TAG_CLOSE)
				{
					// recursive, parse nested elements
					XmlElement child = Xml.parseElement(builder, element);
					// null check
					if (child != null)
					{
						// add child to element
						element.addChild(child);
					}
					token = builder.getLastToken();
				}

				// consume the close tag
				token = builder.extract();

				// expect name tag
				if (token.type != XmlToken.Type.NAME)
				{
					throw new XmlParseException(XmlToken.Type.NAME, token);
				}

				// validate name tag
				if (!token.text.equals(element.getName()))
				{
					throw new XmlParseException("CLOSE_TAG[" + token.text + "] does not match BEGIN_TAG[" + element.getName() + "]"
							+ "at " + token.lineStart + ":" + token.columnStart);
				}

				// consume name tag
				token = builder.extract();

				// expect end tag
				if (token.type != XmlToken.Type.TAG_END)
				{
					throw new XmlParseException(XmlToken.Type.TAG_END, token);
				}

				// consume end tag
				token = builder.extract();
			}

			// after everything return element
			return element;
		}
		else if (token.type == XmlToken.Type.CDATA_BEGIN)
		{
			// consume cdata begin tag
			token = builder.extract();

			// expects text tag
			if (token.type != XmlToken.Type.TEXT)
			{
				throw new XmlParseException(XmlToken.Type.TEXT, token);
			}

			// get contents of text tag
			String cdata = token.text;

			// consume text tag
			token = builder.extract();

			// expect cdata end tag
			if (token.type != XmlToken.Type.CDATA_END)
			{
				throw new XmlParseException(XmlToken.Type.CDATA_END, token);
			}

			// consume cdata end tag
			token = builder.extract();

			// create element
			XmlElement element = new XmlElement(root, "CDATA");

			// create and add attribute
			XmlAttribute attrib = new XmlAttribute("text", cdata);
			element.setAttribute(attrib);

			// return element
			return element;
		}
		else if (token.type == XmlToken.Type.META_BEGIN)
		{
			// consume meta begin tag
			token = builder.extract();

			// expect name tag
			if (token.type != XmlToken.Type.NAME)
			{
				throw new XmlParseException(XmlToken.Type.NAME, token);
			}

			// get content of name tag
			String meta = token.text;

			// create element
			XmlElement element = new XmlElement(root, meta);

			// consume name tag
			token = builder.extract();

			// if name tag then enter loop to parse attributes
			while (token.type == XmlToken.Type.NAME)
			{
				// get content of name tag
				String attribute = token.text;

				// consume name tag
				token = builder.extract();

				// expect equals tag
				if (token.type != XmlToken.Type.EQUALS)
				{
					throw new XmlParseException(XmlToken.Type.EQUALS, token);
				}

				// consume equals tag
				token = builder.extract();

				// expect string tag
				if (token.type != XmlToken.Type.STRING)
				{
					throw new XmlParseException(XmlToken.Type.STRING, token);
				}

				// get content of string tag
				String value = token.text;

				// create and add attribute
				XmlAttribute attrib = new XmlAttribute(attribute, value);
				element.setAttribute(attrib);

				// consume string tag, continue loop if name tag, otherwise exits loop
				token = builder.extract();
			}

			// expect meta close tag
			if (token.type != XmlToken.Type.META_END)
			{
				throw new XmlParseException(XmlToken.Type.META_END, token);
			}

			// consume meta close tag
			token = builder.extract();

			// return element
			return element;
		}
		else if (token.type == XmlToken.Type.COMMENT_BEGIN)
		{
			// consume comment begin tag
			token = builder.extract();

			// expect text tag
			if (token.type != XmlToken.Type.TEXT)
			{
				throw new XmlParseException(XmlToken.Type.TEXT, token);
			}

			// get contents of text tag
			String comment = token.text;

			// consume text tag
			token = builder.extract();

			// expect comment end tag
			if (token.type != XmlToken.Type.COMMENT_END)
			{
				throw new XmlParseException(XmlToken.Type.COMMENT_END, token);
			}

			// consume comment end tag
			token = builder.extract();

			// create element
			XmlElement element = new XmlElement(root, "COMMENT");

			// create and add attribute
			XmlAttribute attrib = new XmlAttribute("text", comment);
			element.setAttribute(attrib);

			// return element
			return element;
		}
		else
		{
			throw new XmlParseException("Expected an open tag, got: " + token.toString());
		}
	}

}
